package modele.createurs;

import modele.Partie;
import modele.entite.Joueur;

/**
 * Point d'extension pour la création d'entité
 */
public abstract class CreateurEntite {
    public abstract Joueur creerJoueur(Partie partie);
    public abstract void creerObstacle(Partie partie);

}
