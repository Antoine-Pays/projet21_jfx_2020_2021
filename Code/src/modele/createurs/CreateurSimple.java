package modele.createurs;

import modele.Partie;
import modele.entite.Entite;
import modele.entite.Joueur;
import modele.entite.PassageObstacle;
import modele.entite.RectangleObstacle;
import stub.Stub;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Créateur qui crée les entités pour un créateur simple
 */
public class CreateurSimple extends CreateurEntite {

    /**
     * Créé un joueur et l'ajoute dans la liste de la partie
     * @param partie partie en cours
     * @return une entité de type joueur
     */
    @Override
    public Joueur creerJoueur(Partie partie) {
        Joueur j = new Joueur(30,20, "/img/birdy.gif");
        partie.ajouterEntite(j);
        return j;

    }

    /**
     * Créé des obstacles et les ajoutent dans la liste de la partie
     * @param partie partie en cours
     */
    @Override
    public void creerObstacle(Partie partie) {

        Random random = new Random();
        int randomYObs1 = 30 + random.nextInt(480);
        int randomYObs2 = randomYObs1 + 100 + random.nextInt(81);

        RectangleObstacle obs2 = new RectangleObstacle(1180, 1280, 0, randomYObs1 );
        PassageObstacle passage1 = new PassageObstacle(1180, 1280, randomYObs1, randomYObs2 );
        RectangleObstacle obs3 = new RectangleObstacle(1180, 1280, randomYObs2, 720 );

        partie.ajouterEntite(obs2);
        partie.ajouterEntite(obs3);
        partie.ajouterEntite(passage1);

    }
}
