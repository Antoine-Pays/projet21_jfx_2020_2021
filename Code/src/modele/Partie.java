package modele;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import modele.entite.Entite;
import modele.gestionCollision.TypeCollision;
import java.util.HashMap;

public class Partie {

    private ObservableList<Entite> lesEntites;
    private HashMap<TypeCollision, Entite> listColl = new HashMap<>();
    private BooleanProperty playing = new SimpleBooleanProperty();
        public BooleanProperty isPlayingProperty() { return playing; }
        public void setPlaying(boolean isPlaying) {
        this.playing.set(isPlaying);
    }

    public boolean isPlaying() { return playing.get(); }


    public Partie() {
        lesEntites = FXCollections.observableArrayList();
        setPlaying(false);
    }

    /**
     * Récupère la liste de collisions
     * @return La HashMap contenant toutes les collisions et les entités associées
     */
    public HashMap<TypeCollision, Entite> getListColl() {
        return listColl;
    }

    /**
     * Permets d'ajouter une collision à la Map
     * @param t Type de collision
     * @param entite Entite avec qui il y a eu une collision
     */
    public void ajouterCollision(TypeCollision t, Entite entite){
        listColl.put(t, entite);
    }

    /**
     * Permets de supprimer une collision à la Map
     * @param t Type de collision
     * @param entite Entite avec qui il y a eu une collision
     */
    public void supprimerCollision(TypeCollision t, Entite entite){
        listColl.remove(t, entite);
    }

    /**
     * Permets d'ajouter une Entite à la liste d'entités de la partie
     * @param e entité à ajouter
     */
    public void ajouterEntite(Entite e){
        lesEntites.add(e);
    }

    /**
     * Permets de supprimer une Entite à la liste d'entités de la partie
     * @param e entité à supprimer
     */
    public void supprimerEntite(Entite e) {
        lesEntites.remove(e);
    }

    /**
     * Récupère la liste d'entités de la partie
     * @return une liste observable d'entités
     */
    public ObservableList<Entite> getLesEntites() {
        return FXCollections.unmodifiableObservableList(lesEntites);
    }


}
