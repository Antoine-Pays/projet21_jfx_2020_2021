package modele;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import modele.persistance.Chargeur;
import modele.persistance.ChargeurBinaire;


public class Connexion  {

    public ObservableList<Utilisateur> mesUsers = FXCollections.observableArrayList();
    public ListProperty<Utilisateur> mesUtilisateurs = new SimpleListProperty<>(mesUsers);
        public ReadOnlyListProperty<Utilisateur> mesUtilisateursProperty() {
            return mesUtilisateurs;
        }
        public ObservableList<Utilisateur> getMesUtilisateurs() { return mesUtilisateurs.get(); }
        public void setMesUtilisateurs(ObservableList<Utilisateur> mesUtilisateurs) { this.mesUtilisateurs.set(mesUtilisateurs); }

    public Connexion()
    {
        Chargeur chargeur = new ChargeurBinaire();
        setMesUtilisateurs(chargeur.charger());
    }

    /**
     * Vérifie si l'utilisateur est bien contenu dans nos données
     * @param pseudo pseudo saisie par l'utilisateur
     * @param motDePasse mot de passe saisie par l'utilisateur
     * @return L'utilisateur si connecté
     */
    public  Utilisateur userConnecte(String pseudo, String motDePasse)
    {
        for ( Utilisateur u : mesUtilisateurs) {
            if(u.getPseudo().equals(pseudo) )
            {
                if(u.getMotDePasse().equals(motDePasse))
                    return u;
                else
                    return null;
            }
        }
        return null;
    }

    /**
     * Ajoute un utilisateur à la liste
     * @param u utilisateur à ajouter
     */
    public void ajouterUtilisateur(Utilisateur u)
    {
        mesUsers.add(u);
    }

}
