package modele.gestionCollision;


import launch.Launch;
import modele.Partie;
import modele.entite.Entite;
import modele.gestionDeplacement.GestionnaireDeplacement;

public class CollisionStandard extends GestionnaireCollision{

    public CollisionStandard(Entite entite) {
       super(entite);
    }
    
    @Override
    public boolean canMove(GestionnaireDeplacement deplacement, Partie partie) {
        for (Entite e : partie.getLesEntites()){
            if (this.entite == e) continue;

            double playerMinX = deplacement.getNewX();
            double playerMinY = deplacement.getNewY();
            double playerMaxX = deplacement.getNewX() + entite.getMaxWidth();
            double playerMaxY = deplacement.getNewY() + entite.getMaxHeight();

            double entMinX = e.getX();
            double entMinY = e.getY();
            double entMaxX = e.getX() + e.getMaxWidth();
            double entMaxY = e.getY() + e.getMaxHeight();

            //Test si l'entité rentre en collision avec n'importe quelle entité présente dans la liste a part elle
            if (playerMinX < entMaxX && playerMaxX > entMinX && playerMinY < entMaxY && playerMaxY > entMinY) {
                partie.ajouterCollision(TypeCollision.COLLISION_SIMPLE, e);
                return false;
            }
        }

        //Collision simple s'il y a collision avec les limites de la fenêtre
        if(!(deplacement.getNewX() >= 0 && deplacement.getNewX() + entite.getMaxWidth() <= (Launch.getLeManager().getLeCollisionneur().getWidth())
                && deplacement.getNewY() >= 0 && deplacement.getNewY() + entite.getMaxHeight() <= (Launch.getLeManager().getLeCollisionneur().getHeight()))) {
            partie.ajouterCollision(TypeCollision.COLLISION_SIMPLE, entite);
            return false;
        }
        return true;
    }
}
