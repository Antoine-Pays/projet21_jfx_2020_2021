package modele.gestionCollision;

/**
 * Enum qui contient toutes les collisions possibles
 */
public enum TypeCollision {
    COLLISION_OBSTACLE_MUR,
    COLLISION_JOUEUR_OBSTACLE,
    COLLISION_JOUEUR_PASSAGE,
    COLLISION_JOUEUR_SOL,
    COLLISION_PASSAGE_MUR,
    COLLISION_SIMPLE
}
