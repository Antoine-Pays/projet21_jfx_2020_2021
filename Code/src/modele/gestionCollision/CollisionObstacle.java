package modele.gestionCollision;


import launch.Launch;
import modele.Partie;
import modele.entite.Entite;
import modele.entite.Joueur;
import modele.gestionDeplacement.GestionnaireDeplacement;

public class CollisionObstacle extends CollisionStandard {

    public CollisionObstacle(Entite entite) {
       super(entite);
    }

    @Override
    public boolean canMove(GestionnaireDeplacement deplacement, Partie partie) {
        for (Entite e : partie.getLesEntites()) {
            if (this.entite == e) continue;

            double playerMinX = deplacement.getNewX();
            double playerMinY = deplacement.getNewY();
            double playerMaxX = deplacement.getNewX() + entite.getMaxWidth();
            double playerMaxY = deplacement.getNewY() + entite.getMaxHeight();

            double entMinX = e.getX();
            double entMinY = e.getY();
            double entMaxX = e.getX() + e.getMaxWidth();
            double entMaxY = e.getY() + e.getMaxHeight();

            //Vérifie si l'entité ici normalement un obstacle entre en collision avec un joueur
            if (playerMinX < entMaxX && playerMaxX > entMinX && playerMinY < entMaxY && playerMaxY > entMinY) {
                if (e instanceof Joueur) {
                    partie.ajouterCollision(TypeCollision.COLLISION_JOUEUR_OBSTACLE, entite);
                    return false;
                }
            }}

        //Vérifie si l'entité ici normalement un obstacle entre en collision avec un bord de l'application
        if(!(deplacement.getNewX() >= 0 && deplacement.getNewX() + entite.getMaxWidth() <= (Launch.getLeManager().getLeCollisionneur().getWidth())
                && deplacement.getNewY() >= 0 && deplacement.getNewY() + entite.getMaxHeight() <= (Launch.getLeManager().getLeCollisionneur().getHeight()))) {
            partie.ajouterCollision(TypeCollision.COLLISION_OBSTACLE_MUR, entite);
            return false;
        }
        return super.canMove(deplacement, partie);
    }
}
