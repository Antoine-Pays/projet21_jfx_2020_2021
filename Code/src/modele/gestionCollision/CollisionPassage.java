package modele.gestionCollision;


import launch.Launch;
import modele.Partie;
import modele.entite.Entite;
import modele.entite.Joueur;
import modele.gestionDeplacement.GestionnaireDeplacement;

public class CollisionPassage extends CollisionStandard {

    public CollisionPassage(Entite entite) {
       super(entite);
    }

    @Override
    public boolean canMove(GestionnaireDeplacement deplacement, Partie partie) {
        for (Entite e : partie.getLesEntites()){
            if (this.entite == e) continue;

            double localEntityMinX = deplacement.getNewX();
            double localEntityMinY = deplacement.getNewY();
            double localEntityMaxX = deplacement.getNewX() + entite.getMaxWidth();
            double localEntityMaxY = deplacement.getNewY() + entite.getMaxHeight();

            double entMinX = e.getX();
            double entMinY = e.getY();
            double entMaxX = e.getX() + e.getMaxWidth();
            double entMaxY = e.getY() + e.getMaxHeight();

            //Vérifie si l'entité ici normalement un passage entre en collision avec un joueur
            if (localEntityMinX < entMaxX && localEntityMaxX > entMinX && localEntityMinY < entMaxY && localEntityMaxY > entMinY) {
                if (e instanceof Joueur) {
                    partie.ajouterCollision(TypeCollision.COLLISION_JOUEUR_PASSAGE, entite);
                    return false;
                }
            }
        }

        //Vérifie si l'entité ici normalement un passage entre en collision avec le bord de l'application
        if(!(deplacement.getNewX() >= 0 && deplacement.getNewX() + entite.getMaxWidth() <= (Launch.getLeManager().getLeCollisionneur().getWidth())
                && deplacement.getNewY() >= 0 && deplacement.getNewY() + entite.getMaxHeight() <= (Launch.getLeManager().getLeCollisionneur().getHeight()))) {
            partie.ajouterCollision(TypeCollision.COLLISION_PASSAGE_MUR, entite);
            return false;
        }

        return super.canMove(deplacement, partie);
    }
}




