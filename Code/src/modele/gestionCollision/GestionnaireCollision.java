package modele.gestionCollision;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import modele.Partie;
import modele.entite.Entite;
import modele.gestionDeplacement.GestionnaireDeplacement;

/**
 * Gestionnaire qui est un point d'extension pour toutes les collisions du jeu
 */
public abstract class GestionnaireCollision {

    protected Entite entite;

    public GestionnaireCollision(Entite entite) {
        this.entite = entite;
    }

    protected DoubleProperty width = new SimpleDoubleProperty();
        public double getWidth() {return width.get();}
        public DoubleProperty widthProperty() {
        return width;
    }
        public void setWidth(double width) {
        this.width.set(width);
    }

    protected DoubleProperty height = new SimpleDoubleProperty();
        public double getHeight() {
        return height.get();
    }
        public DoubleProperty heightProperty() {
        return height;
    }
        public void setHeight(double height) {
        this.height.set(height);
    }

    /**
     * Permets de retourner si l'entité entre en collision et peut bouger
     * @param deplacement Type de déplacement choisi
     * @param partie partie en cours
     * @return un booleen si l'entité peut se déplacer ou pas
     */
    public abstract boolean canMove(GestionnaireDeplacement deplacement, Partie partie);

}
