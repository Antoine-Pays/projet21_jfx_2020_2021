package modele.gestionCollision;

import launch.Launch;
import modele.Partie;
import modele.entite.Entite;
import modele.entite.PassageObstacle;
import modele.entite.RectangleObstacle;
import modele.gestionDeplacement.GestionnaireDeplacement;

public class CollisionJoueur extends CollisionStandard {

    public CollisionJoueur(Entite entite) {
       super(entite);
    }

    @Override
    public boolean canMove(GestionnaireDeplacement deplacement, Partie partie) {
        for (Entite e : partie.getLesEntites()){
            if (this.entite == e) continue;

            double playerMinX = deplacement.getNewX();
            double playerMinY = deplacement.getNewY();
            double playerMaxX = deplacement.getNewX() + entite.getMaxWidth();
            double playerMaxY = deplacement.getNewY() + entite.getMaxHeight();

            double entMinX = e.getX();
            double entMinY = e.getY();
            double entMaxX = e.getX() + e.getMaxWidth();
            double entMaxY = e.getY() + e.getMaxHeight();

            //Vérifie si l'entité ici normalement un joueur entre en collision avec un passage ou un obstacle
            if (playerMinX < entMaxX && playerMaxX > entMinX && playerMinY < entMaxY && playerMaxY > entMinY) {
                if (e instanceof PassageObstacle) {
                    partie.ajouterCollision(TypeCollision.COLLISION_JOUEUR_PASSAGE, e);
                    return false;

                } else if (e instanceof RectangleObstacle) {
                    partie.ajouterCollision(TypeCollision.COLLISION_JOUEUR_OBSTACLE, e);
                    return false;
                } else {
                    partie.ajouterCollision(TypeCollision.COLLISION_SIMPLE, e);
                    return false;
                }
            }
        }

        if(!(deplacement.getNewY() + entite.getMaxHeight() <= (Launch.getLeManager().getLeCollisionneur().getHeight()))) {
            partie.ajouterCollision(TypeCollision.COLLISION_JOUEUR_SOL, entite);
            return false;
        }

        return super.canMove(deplacement, partie);
    }
}




