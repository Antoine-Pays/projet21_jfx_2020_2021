package modele;

import boucle.Boucleur;
import boucle.BoucleurJeu;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ObservableList;
import javafx.scene.input.KeyEvent;
import modele.createurs.CreateurSimple;
import modele.createurs.CreateurEntite;
import modele.entite.Entite;
import modele.entite.Joueur;
import modele.gestionCollision.CollisionStandard;
import modele.gestionCollision.GestionnaireCollision;
import modele.gestionCollision.TypeCollision;
import modele.gestionDeplacement.*;
import modele.gestionnaireActionsCollision.GestionnaireActionCollision;
import modele.gestionnaireActionsCollision.ActionCollisionStandard;
import modele.persistance.Chargeur;
import modele.persistance.ChargeurBinaire;
import modele.persistance.Sauvegarde;
import modele.persistance.SauvegardeBinaire;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Manager implements InvalidationListener {

    private Entite joueurEnCours;
    private Connexion con;
    private Utilisateur userCourant;
    private CreateurEntite leCreateur;
    private GestionnaireCollision leGestionnaireColl;
    private GestionnaireActionCollision leGestionnaireSupp;
    private Boucleur leBoucleur;
    private Partie partie;
    private int cpt;
    private Direction directionEnCours; //Touches

    private IntegerProperty pointsEnCours  = new SimpleIntegerProperty();
        public int getPointsEnCours() {
        return pointsEnCours.get();
    }
        public IntegerProperty pointsEnCoursProperty() { return pointsEnCours; }
        public void setPointsEnCours(int pointsEnCours) {
        this.pointsEnCours.set(pointsEnCours);
    }


    public Manager(Connexion con)
    {
        this.con = con;
    }


    /**
     * Méthode s'effectuant à chaque tour de boucle
     */
    @Override
    public void invalidated(Observable observable) {

        //Crée des obstacles toutes les 3 secondes
        if (cpt%100 == 0){
            leCreateur.creerObstacle(partie);
        }

        //Instancie un déplacement joueur et déplace le joueur
        GestionnaireDeplacement deplacementJoueur = new DeplacementJoueur(directionEnCours, partie);
        deplacementJoueur.deplacer(joueurEnCours);

        //Instancie un déplacement obstacle et le déplace
        GestionnaireDeplacement deplacementObstacle = new DeplacementObstacle(partie);
        for (Entite e : partie.getLesEntites()){
            if (e instanceof Joueur) { continue; }

            deplacementObstacle.deplacer(e);

        }

        //Itérateur qui permet de gérer toutes les collisions de la partie
        Iterator<Map.Entry<TypeCollision, Entite>> iterator = partie.getListColl().entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<TypeCollision, Entite> laMapColl = iterator.next();
            if ((laMapColl.getKey() == TypeCollision.COLLISION_JOUEUR_OBSTACLE) || (laMapColl.getKey() == TypeCollision.COLLISION_JOUEUR_SOL)) {
                leGestionnaireSupp.actionCollJoueurObs(laMapColl.getValue());
            }
            else if (laMapColl.getKey() == TypeCollision.COLLISION_JOUEUR_PASSAGE) {
                leGestionnaireSupp.actionCollJoueurPassage(laMapColl.getValue());
            }
            else if ((laMapColl.getKey() == TypeCollision.COLLISION_OBSTACLE_MUR) || (laMapColl.getKey() == TypeCollision.COLLISION_PASSAGE_MUR)) {
                leGestionnaireSupp.actionCollMur(laMapColl.getValue());
            }

            iterator.remove();
        }
        cpt = cpt + 1;
    }

    /**
     * Instancie tous les éléments nécessaires pour lancer une partie
     */
    public void creationPartie(){
        leCreateur = new CreateurSimple();
        this.partie = new Partie();
        leGestionnaireColl = new CollisionStandard(null);
        leGestionnaireSupp = new ActionCollisionStandard(partie);
        joueurEnCours = leCreateur.creerJoueur(partie);
        partie.setPlaying(true);

        this.leBoucleur = new BoucleurJeu();
        this.leBoucleur.addListener(this);
        this.leBoucleur.setActif(true);

        new Thread(this.leBoucleur).start();
    }

    /**
     * Stop la boucle de la partie
     */ 
    public void stopBoucle(){
        leBoucleur.setActif(false);
    }

    /**
     * Récupère le joueur en cours
     * @return Une entité de type joueur
     */
    public Entite getJoueurEnCours() {
        return joueurEnCours;
    }

    /**
     * Récupère la partie en cours
     * @return La partie en cours
     */
    public Partie getPartie() {
        return partie;
    }

    /**
     * Gère et fait des actions en fonction de la touche pressé
     * @param code code de la touche qui a été pressé
     */
    public void startDeplace(KeyEvent code){
        switch(code.getCode()){

            case LEFT :
                directionEnCours = Direction.GAUCHE;
                break;

            case RIGHT :
                directionEnCours = Direction.DROITE;
                break;

            case UP:
                directionEnCours = Direction.HAUT;
                break;

            case DOWN:
                directionEnCours = Direction.BAS;
                break;

            default:
                directionEnCours=null;
                break;

        }
    }

    public void sauvegarder (List<Utilisateur> mesUtilisateurs)
    {
        Sauvegarde s = new SauvegardeBinaire();
        s.save(mesUtilisateurs);
    }

    public ObservableList<Utilisateur> charger()
    {
        Chargeur chargeur = new ChargeurBinaire();
        ObservableList<Utilisateur> mesUtilisateurs = chargeur.charger();
        return mesUtilisateurs;
    }

    /**
     * Stop le déplacement lorsque la touche est lâchée
     * @param code code de la touche qui est lâchée
     */
    public void stopDeplace(KeyEvent code){
        directionEnCours = null;
    }

    /**
     * Récupère le collisionneur instancié
     * @return Un gestionnaire de collision
     */
    public GestionnaireCollision getLeCollisionneur() {
        return leGestionnaireColl;
    }

    /**
     * Permet d'assigner le meilleur score
     * @param points Valeur à assigner
     */
    public void setHighScore(int points) { getUserCourant().setRecord(points); }

    /**
     * Récupère la connexion
     * @return Une connexion
     */
    public Connexion getCon() {
        return con;
    }

    /**
     * Récupère la liste d'entités présente dans la partie
     * @return Une liste observable
     */
    public ObservableList<Entite> getEntites()
    {
        return getPartie().getLesEntites();
    }

    /**
     * Récupère l'utilisateur en cours
     * @return Un utilisateur
     */
    public Utilisateur getUserCourant() {
        return userCourant;
    }

    /**
     * Permet d'assigner un utilisateur courant
     * @param userCourant utilisateur à assigner
     */
    public void setEnCours(Utilisateur userCourant) {
        this.userCourant = userCourant;
    }

    /**
     * Permets de se connecter et de vérifier si l'utilisateur est bien créé et l'assigne comme en cours
     * @param pseudo pseudo saisi par l'utilisateur
     * @param mdp mot de passe saisi par l'utilisateur
     * @return Retourne un booleen de si la vérification s'est effectuée ou pas
     */
    public Boolean seConnecter(String pseudo, String mdp)
    {
        Utilisateur user = con.userConnecte(pseudo,mdp);
        if(user !=null)
        {
            this.setEnCours(user);
            return true;
        }
        return false;

    }


}
