package modele;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import java.io.Serializable;

public class Utilisateur implements Serializable {

    private String motDePasse;

    private SimpleStringProperty image = new SimpleStringProperty();
        public SimpleStringProperty imageProperty() {
        return image;
    }
        public void setImage(String image) {
        this.image.set(image);
    }
        public String getImage() {
        return image.get();
    }

    private StringProperty pseudo = new SimpleStringProperty();
        public StringProperty pseudoProperty()
    {
        return pseudo;
    }
        public String getPseudo() {
        return pseudo.getValue();
    }

    private SimpleIntegerProperty record  = new SimpleIntegerProperty();
         public SimpleIntegerProperty recordProperty() { return record; }
         public int getRecord() {
            return record.get();
        }
         public void setRecord(int record) { this.record.set(Math.max(record,this.record.get())); }

    public Utilisateur( String pseudo, String motDePAsse, String image) {
        this.pseudo.set(pseudo);
        this.motDePasse = motDePAsse;
        this.record.set(0);
        this.setImage(image);

    }

    /**
     * Récupère le mot de passe de l'utilisateur
     * @return Retourne un string contenant le mot de passe
     */
    public String getMotDePasse() {
        return motDePasse;
    }

    /**
     * Redifinition du equals pour comparer un utilisateur avec son pseudo et son mot de passe
     * @param o utilisateur à comparer
     * @return renvoie si le equals est valide ou non
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Utilisateur that = (Utilisateur) o;
        return motDePasse.equals(that.motDePasse) &&
                pseudo.get().equals(that.pseudo.get());
    }

    @Override
    public String toString() {
        String s = getPseudo() + "      "  + getRecord();
        return s;
    }

}


