package modele.gestionnaireActionsCollision;

import launch.Launch;
import modele.Partie;
import modele.entite.Entite;

public class ActionCollisionStandard extends GestionnaireActionCollision {

    public ActionCollisionStandard(Partie partie) {
        super(partie);
    }

    @Override
    public void actionCollJoueurObs(Entite entite) {

        partie.setPlaying(false);
    }

    @Override
    public void actionCollJoueurPassage(Entite entite) {
        int points =  Launch.getLeManager().getPointsEnCours();
        Launch.getLeManager().setPointsEnCours(points+1);
        partie.supprimerEntite(entite);
    }

    @Override
    public void actionCollMur(Entite entite) {
        partie.supprimerEntite(entite);
    }
}
