package modele.gestionnaireActionsCollision;


import modele.Partie;
import modele.entite.Entite;

public abstract class GestionnaireActionCollision {

    protected Partie partie;

    public GestionnaireActionCollision(Partie partie) {
        this.partie = partie;
    }

    /**
     * Effectue une action sur l'entité obstacle rentrée en contact avec le joueur
     * @param entite Entité correspondant dans ce cas à un obstacle
     */
    public abstract void actionCollJoueurObs(Entite entite);

    /**
     * Effectue une action sur l'entité passage rentrée en contact avec le joueur
     * @param entite Entité correspondant dans ce cas à un passage
     */
    public abstract void actionCollJoueurPassage(Entite entite);

    /**
     * Effectue une action lors d'une collision avec le mur
     * @param entite Entité entrée en contact
     */
    public abstract void actionCollMur(Entite entite);
}
