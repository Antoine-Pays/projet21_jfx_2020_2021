package modele.gestionDeplacement;

import modele.Partie;
import modele.entite.Entite;

public abstract class GestionnaireDeplacement {

    protected Direction orientation;
    protected Partie partie;

    public GestionnaireDeplacement(Direction dir, Partie partie) {
        this.orientation = dir;
        this.partie = partie;
    }

    /**
     * Fonction qui permet à une entité de se déplacer
     * @param e Entité à déplacer
     */
    public abstract void deplacer(Entite e);

    /**
     * Récupère le nouveau X que l'entité aura après le déplacement
     * @return Valeur du nouveau X
     */
    public abstract double getNewX();

    /**
     * Récupère le nouveau Y que l'entité aura après le déplacement
     * @return Valeur du nouveau Y
     */
    public abstract double getNewY();

    /**
     * Récupère la valeur actuel X de l'entité
     * @return Valeur du X
     */
    public abstract double getOldX();

    /**
     * Récupère la valeur actuel Y de l'entité
     * @return Valeur du Y
     */
    public abstract double getOldY();

    /**
     * Récupère l'orientation voulu du déplacement
     * @return Une direction listé dans l'enum
     */
    public abstract Direction getOrientation();

}
