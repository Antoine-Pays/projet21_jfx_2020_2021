package modele.gestionDeplacement;


import modele.Partie;
import modele.entite.Entite;

public  class DeplacementStandard extends GestionnaireDeplacement {

    protected double oldX;
    protected double oldY;
    protected double newX;
    protected double newY;

    public DeplacementStandard(Direction dir, Partie partie) {
        super(dir, partie);
    }

    @Override
    public void deplacer(Entite e) { }

    public double getOldX() {
        return oldX;
    }

    public double getOldY() {
        return oldY;
    }

    public double getNewX() {
        return newX;
    }

    public double getNewY() {
        return newY;
    }

    public Direction getOrientation() { return orientation; };
}
