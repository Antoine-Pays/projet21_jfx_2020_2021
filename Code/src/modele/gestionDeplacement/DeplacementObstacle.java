package modele.gestionDeplacement;
import modele.Partie;
import modele.entite.Entite;

public class DeplacementObstacle extends DeplacementStandard {

    public DeplacementObstacle(Partie partie) {
        super(Direction.GAUCHE, partie);
    }

    @Override
    public void deplacer(Entite e) {

        if(this.getOrientation() == null)   return;

        oldX = e.getX();
        oldY = e.getY();
        newX = e.getX() - e.getVitesse();
        newY = e.getY();

        if (e.getLaCollision().canMove(this, partie)) {
            e.setX(newX);
            e.setY(newY);
        }
    }
}
