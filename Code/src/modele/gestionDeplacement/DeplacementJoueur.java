package modele.gestionDeplacement;
import modele.Partie;
import modele.entite.Entite;

public class DeplacementJoueur extends DeplacementStandard {

    public DeplacementJoueur(Direction dir, Partie partie) {
        super(dir, partie);
    }

    @Override
    public void deplacer(Entite e) {

        oldX = e.getX();
        oldY = e.getY();
        newX = (orientation == Direction.GAUCHE || orientation == Direction.DROITE) ? (orientation == Direction.GAUCHE ? e.getX() - e.getVitesse() : e.getX() + e.getVitesse()) : e.getX();
        newY = (orientation == Direction.HAUT || orientation == Direction.BAS) ? (orientation == Direction.HAUT ? e.getY() - e.getVitesse() : e.getY() + e.getVitesse()) : e.getY();

        if(orientation!=Direction.HAUT) {
            newY = newY + e.getGravite();
        }

        if (e.getLaCollision().canMove(this, partie)) {
            e.setX(newX);
            e.setY(newY);
        }
    }
}
