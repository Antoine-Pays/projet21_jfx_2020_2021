package modele.gestionDeplacement;

/**
 * Enum qui contient toutes les directions possible
 */

public enum Direction {
    HAUT,
    BAS,
    GAUCHE,
    DROITE
}
