package modele.entite;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import modele.gestionCollision.CollisionStandard;
import modele.gestionCollision.GestionnaireCollision;

/**
 * Une entité qui est un point d'extension pour toutes les entités du jeu
 */
public abstract class Entite {

    protected DoubleProperty x = new SimpleDoubleProperty();
        public DoubleProperty xProperty() { return x; }
        public double getX() {
        return x.get();
    }
        public void setX(double x) { this.x.set(x); }

    protected DoubleProperty y = new SimpleDoubleProperty();
        public DoubleProperty yProperty() { return y; }
        public double getY() {
        return y.get();
    }
        public void setY(double y) { this.y.set(y); }

    protected String image;
    protected int maxHeight;
    protected int maxWidth;
    protected double vitesse = 10;
    protected double gravite = 6;
    protected GestionnaireCollision laCollision;


    public Entite() {}

    public Entite(double x, double y, String image, int maxWidth, int maxHeight) {
        this.x.set(x);
        this.y.set(y);
        this.image = image;
        this.maxHeight = maxHeight;
        this.maxWidth = maxWidth;
        this.laCollision = new CollisionStandard(this);
    }

    /**
     * Sert à obtenir l'image de l'entité
     * @return retourne l'image de l'entité
     */
    public String getImage() {
        return image;
    }

    /**
     * Sert à obtenir la collision de l'entité
     * @return retourne la collision de l'entité
     */
    public GestionnaireCollision getLaCollision() {
        return laCollision;
    }

    /**
     * Sert à obtenir la hauteur de l'entité
     * @return retourne la hauteur de l'entité
     */
    public int getMaxHeight() {
        return maxHeight;
    }

    /**
     * Sert à obtenir la largeur de l'entité
     * @return retourne la largeur de l'entité
     */
    public int getMaxWidth() {
        return maxWidth;
    }

    /**
     * Sert à obtenir la vitesse de l'entité
     * @return retourne la vitesse de l'entité
     */
    public double getVitesse() {
        return vitesse;
    }

    /**
     * Sert à assigner une valeur à la vitesse de l'entité
     * @param vitesse vitesse à assigner
     */


    /**
     * Sert à obtenir la valeur de la gravité de l'entité
     * @return retourne la valeur de la gravité
     */
    public double getGravite() {
        return gravite;
    }
}
