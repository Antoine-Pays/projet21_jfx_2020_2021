package modele.entite;
import modele.gestionCollision.CollisionJoueur;

public class Joueur extends Entite{



    public Joueur(double x, double y, String image){
        super(x, y,image, 30, 40);
        this.laCollision = new CollisionJoueur(this);
    }

    public double getGravite() {
        return gravite;
    }

}
