package modele.entite;
import modele.gestionCollision.CollisionPassage;


public class PassageObstacle extends Entite {

    public PassageObstacle(int x1, int x2, int y1, int y2) {
        this.x.set(x1);
        this.y.set(y1);
        this.maxHeight = (y2 - y1);
        this.maxWidth = (x2 - x1);
        this.laCollision = new CollisionPassage(this);
        vitesse = 8;

    }


}
