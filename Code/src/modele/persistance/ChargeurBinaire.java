package modele.persistance;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import modele.Utilisateur;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ChargeurBinaire extends Chargeur implements IPersistanceParam{

    @Override
    public ObservableList<Utilisateur> charger(){
        ObservableList<Utilisateur> users= FXCollections.observableArrayList();
        try{
            File file = new File(FILE_NAME);
            if(!file.exists())
            {
                throw new Exception("Le fichier n'existe pas");
            }
            FileInputStream f = new FileInputStream(FILE_NAME);
            BufferedInputStream buff = new BufferedInputStream(f);
            ObjectInputStream o = new ObjectInputStream(buff);

            String pseudo= (String)o.readObject();

            while(pseudo!=null) //tant qu'il y a qlqch à lire
            {
                String motDePAsse=(String) o.readObject();
                int points = (int)o.readObject();
                String image=(String) o.readObject();
                Utilisateur u = new Utilisateur(pseudo,motDePAsse,image);
                u.setRecord(points);
                users.add(u);
                 pseudo= (String)o.readObject();

            }
        }
        catch(Exception e){

        }
        return users.sorted(Comparator.comparingInt(Utilisateur::getRecord).reversed());
    }


}
