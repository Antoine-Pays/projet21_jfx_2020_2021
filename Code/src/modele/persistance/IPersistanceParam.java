package modele.persistance;

/**
 * Interface qui contient le nom du fichier de persistance
 */
public interface IPersistanceParam {
    String FILE_NAME = "persistance.ser";
}
