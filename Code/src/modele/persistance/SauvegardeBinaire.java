package modele.persistance;

import modele.Utilisateur;

import java.io.*;
import java.util.List;

public class SauvegardeBinaire extends Sauvegarde implements IPersistanceParam {

    @Override
    public void save(List<Utilisateur> mesUtilisateurs) {
        try {
            FileOutputStream file = new FileOutputStream(FILE_NAME); //ouvrir un tube en écriture
            BufferedOutputStream buff = new BufferedOutputStream(file);
            ObjectOutputStream o  = new ObjectOutputStream(buff);
            for (Utilisateur u : mesUtilisateurs) {
                o.writeObject(u.getPseudo());
                o.writeObject(u.getMotDePasse());
                o.writeObject(u.getRecord());
                o.writeObject(u.getImage());
            }
            o.close();
            buff.close(); //ou bien fermer le file tout court
            file.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
