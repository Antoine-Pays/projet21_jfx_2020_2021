package modele.persistance;

import modele.Utilisateur;

import java.util.List;

public abstract class Sauvegarde {
    /**
     * Permets de sauvegarder la liste d'utilisateurs
     * @param mesUtilisateurs La liste d'utilisateurs à sauvegarder
     */
    public abstract void save(List<Utilisateur> mesUtilisateurs);
}
