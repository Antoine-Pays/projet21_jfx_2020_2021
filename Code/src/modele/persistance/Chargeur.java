package modele.persistance;

import javafx.collections.ObservableList;
import modele.Utilisateur;

import java.util.List;

public abstract class Chargeur {

    /**
     * Permets de charger la liste d'utilisateurs qui a été persisté
     * @return La liste d'utilisateurs qui a été sauvegardé
     */
    public abstract ObservableList<Utilisateur> charger();

}
