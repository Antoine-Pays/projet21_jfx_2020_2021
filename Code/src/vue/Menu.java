package vue;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import launch.Launch;
import modele.Manager;

import static javafx.application.Platform.exit;


public class Menu {

    private Manager leManager = Launch.getLeManager();

    @FXML
    Label pseudoJoueur;

    @FXML
    TextField pseudo,mdp;


    public void initialize() {


    }

    /**
     * Lance une partie à l'appuie du boutton
     * @param actionEvent
     */
    @FXML
    private void lancementPartie(ActionEvent actionEvent) {

        String p = pseudo.getText();
        String m = mdp.getText();

        if (leManager.seConnecter(p, m)) {

            //créé la partie
            leManager.creationPartie();
            //Créé la scene de jeu
            try {
                Launch.setJeu(new Scene(FXMLLoader.load(getClass().getResource("/Jeu.fxml"))));
            } catch (Exception e) {
                e.printStackTrace();
            }

            //Filter pour les touches
            Launch.getJeu().addEventFilter(KeyEvent.KEY_PRESSED, leManager::startDeplace);
            Launch.getJeu().addEventFilter(KeyEvent.KEY_RELEASED, leManager::stopDeplace);

            //Bind pour la taille de la fenêtre de jeu
            leManager.getLeCollisionneur().heightProperty().bind(Launch.getJeu().heightProperty());
            leManager.getLeCollisionneur().widthProperty().bind(Launch.getJeu().widthProperty());

            Launch.getWindow().setScene(Launch.getJeu());

        } else {
            popUp();
        }

    }

    /**
     * Popup quand l'utilisateur rentre un mauvais couple pseudo/mot de passe
     */
    public void popUp() {

        VBox dialogVbox = new VBox(5);
        dialogVbox.setAlignment(Pos.CENTER);
        dialogVbox.getChildren().add(new Text("Identifiants erronés ! "));
        Scene dialogScene = new Scene(dialogVbox,370,20);
        Stage popUp= new Stage();
        popUp.setTitle("Mot de passe oublié ? ");
        popUp.setScene(dialogScene);
        popUp.show();

    }

    /**
     * Affiche le tableau des scores
     * @param mouseEvent
     */
    @FXML
    private void listeRecord(MouseEvent mouseEvent) {
        try {
            Launch.getWindow().setScene(new Scene(FXMLLoader.load(getClass().getResource("/ListeUtilisateur.fxml"))));

        } catch (Exception e) {
            e.printStackTrace();
        }




    }

    public void quitter(MouseEvent mouseEvent) {
        exit();
    }

}