package vue;

import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.converter.NumberStringConverter;
import launch.Launch;
import modele.Manager;
import modele.entite.Entite;
import modele.entite.Joueur;
import modele.entite.RectangleObstacle;

import java.io.IOException;
import java.util.Iterator;


public class Jeu {

    private Manager leManager = Launch.getLeManager();

    @FXML
    private Text score;

    @FXML
    private Pane jeu;


    public void initialize() {

        //Bind du score du joueur sur la partie actuelle
        score.textProperty().bindBidirectional(leManager.pointsEnCoursProperty(), new NumberStringConverter());

        //Affiche les entités contenues dans la liste au début de la partie
        for (Entite entite : leManager.getPartie().getLesEntites()){
            updateEcran(entite);
        }

        //Listener sur le booleen de fin de partie
        leManager.getPartie().isPlayingProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                leManager.stopBoucle();
                leManager.setHighScore(leManager.getPointsEnCours());
                try {
                    Launch.getWindow().setScene(new Scene(FXMLLoader.load(getClass().getResource("/ListeUtilisateur.fxml"))));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                leManager.sauvegarder(leManager.getCon().getMesUtilisateurs());

            }

        });

        //Listener sur la liste d'entités pour actualiser l'affichage si ajout ou suppression
        leManager.getEntites().addListener((ListChangeListener.Change<? extends Entite> change) -> {
                change.next();
                for (Entite entite : change.getAddedSubList()){
                    updateEcran(entite);
                }
                for (Entite entite : change.getRemoved()) {
                    Iterator<Node> iterator = jeu.getChildren().iterator();
                    while (iterator.hasNext()) {
                        Node leNode = iterator.next();
                        if (leNode.getUserData() == entite) {
                            iterator.remove();
                        }
                    }
                }
        });
    }

    /**
     * Permet de créé l'image, rectangle ou autre pour une entité spécifique
     * @param entite entité à dessiner
     */
    private void updateEcran(Entite entite) {

        if (entite instanceof Joueur){
            ImageView entiteAffiche = new ImageView();

            entiteAffiche.setUserData(entite);
            Image image = new Image(getClass().getResource(leManager.getUserCourant().getImage()).toString());
            entiteAffiche.setImage(image);
            entiteAffiche.layoutXProperty().bind(entite.xProperty());
            entiteAffiche.layoutYProperty().bind(entite.yProperty());
            entiteAffiche.setFitHeight(entite.getMaxHeight());
            entiteAffiche.setFitWidth(entite.getMaxWidth());
            jeu.getChildren().add(entiteAffiche);

        } else if (entite instanceof RectangleObstacle){
            Rectangle entiteAffiche = new Rectangle();
            entiteAffiche.setUserData(entite);
            entiteAffiche.layoutXProperty().bind(entite.xProperty());
            entiteAffiche.layoutYProperty().bind(entite.yProperty());
            entiteAffiche.setHeight(entite.getMaxHeight());
            entiteAffiche.setWidth(entite.getMaxWidth());
            jeu.getChildren().add(entiteAffiche);
        }

    }

}
