package vue;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import modele.Utilisateur;


public class CelluleUtilisateur extends ListCell<Utilisateur> {

    /**
     * Redéfinis une Cellule personnalisée
     * @param item utilisateur
     * @param empty
     */
    @Override
    protected void updateItem(Utilisateur item, boolean empty) {
        super.updateItem(item, empty);
        if(item !=null)
        {
            HBox container = new HBox();
            ImageView imageView = new ImageView();
            Image image = new Image(String.valueOf(getClass().getResource(item.getImage())));
            imageView.setImage(image);
            imageView.setFitHeight(30);
            imageView.setFitWidth(30);
            Label lb = new Label();
            lb.textProperty().bind(item.pseudoProperty());
            Label record = new Label();
            record.textProperty().bind(item.recordProperty().asString());
            container.getChildren().addAll(imageView,lb,record);
            container.setSpacing(10);
            setGraphic(container);
        }
        else
        {
            setGraphic(null);
        }

    }
}
