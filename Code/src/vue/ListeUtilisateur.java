package vue;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import launch.Launch;
import modele.Manager;
import modele.Utilisateur;
import modele.persistance.Sauvegarde;
import modele.persistance.SauvegardeBinaire;


public class ListeUtilisateur {

    private Manager leManager = Launch.getLeManager();


    @FXML
    private ListView<Utilisateur> recordListe;

    @FXML
    private TextField text;




    public void initialize() {

        recordListe.getSelectionModel().selectFirst();

        //Bind de la listeView sur la liste des utilisateurs
        recordListe.itemsProperty().bind(Launch.getLeManager().getCon().mesUtilisateursProperty());

        //Permet d'afficher le texte bind selon l'item sélectionné sans changer son contenue à chaque clique
        recordListe.getSelectionModel().selectedItemProperty().addListener(
                (__, utilisateur1, t1) -> {
                    if(utilisateur1 !=null) {
                        text.textProperty().unbindBidirectional(utilisateur1.pseudoProperty());
                    }
                    if(t1 !=null) {
                        text.textProperty().bindBidirectional(t1.pseudoProperty());
                    }
                }
        );

        //Permet de spécialiser l'affichage de la liste des utilisateurs
        recordListe.setCellFactory(__ -> new CelluleUtilisateur());

    }



    /**
     * Permet l'affichage du menu
     * @param mouseEvent
     */

    @FXML
    private void retourMenu(MouseEvent mouseEvent) {
       Launch.getWindow().setScene(Launch.getMenu());

    }
}

