package stub;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import modele.Utilisateur;
import modele.entite.*;
import modele.persistance.Sauvegarde;
import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class Stub extends Sauvegarde {

    /**
     * Permet d'instancier un joueur
     * @return Un joueur qui a été instancié
     */
    public static Joueur creationJoueur(){
        Joueur j = new Joueur(30,20, "/img/birdy.gif");
        return j;
    }

    /**
     * Instancie des entités de type obstacle et passageObstacle
     * @return Une liste d'entités contenant des obstacles et un passage
     */
    public static List<Entite> createurObstacles()
    {
        List<Entite> liste = new ArrayList<>();

        Random random = new Random();
        int randomYObs1 = 30 + random.nextInt(601);
        int randomYObs2 = randomYObs1 + 100 + random.nextInt(81);

        RectangleObstacle obs2 = new RectangleObstacle(1180, 1280, 0, randomYObs1 );
        PassageObstacle passage1 = new PassageObstacle(1180, 1280, randomYObs1, randomYObs2 );
        RectangleObstacle obs3 = new RectangleObstacle(1180, 1280, randomYObs2, 720 );

        //Obstacle obs1 = new Obstacle(350,random.nextInt(300), Couleur.rouge,"/img/carrerouge.jpg");

        liste.add(obs2);
        liste.add(obs3);
        liste.add(passage1);

        return liste;

    }

    /**
     * Instancie des utilisateurs
     * @return Une liste observable d'utilisateurs
     */
    public static ObservableList<Utilisateur> creerUtilisateurs()   {
        Utilisateur kami = new Utilisateur("kam","kam","/img/birdy.gif");
        Utilisateur anto  = new Utilisateur("antoine","anto","/img/bird.png");
        Utilisateur meriem  = new Utilisateur("meriem","meriem","/img/birdy.gif");
        Utilisateur cedric  = new Utilisateur("cedric","cedric","/img/bird.png");

        ObservableList<Utilisateur> mesJoueurs= FXCollections.observableArrayList();
        mesJoueurs.add(kami);
        mesJoueurs.add(anto);
        mesJoueurs.add(meriem);
        mesJoueurs.add(cedric);

        return mesJoueurs.sorted(Comparator.comparingInt(Utilisateur::getRecord));


    }

    @Override
    public void save(List<Utilisateur> mesUtilisateurs){
        try {
            FileOutputStream file = new FileOutputStream("serialisation.ser"); //ouvrir un tube en écriture
            BufferedOutputStream buff = new BufferedOutputStream(file);
            ObjectOutputStream o  = new ObjectOutputStream(buff);
            o.writeObject(mesUtilisateurs);
            o.close();
            buff.close(); //ou bien fermer le file
            file.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
