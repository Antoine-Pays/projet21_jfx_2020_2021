package launch;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import modele.Connexion;
import modele.Manager;


public class Launch extends Application {

    private static Connexion connexion= new Connexion();
    private static Manager leManager = new Manager(connexion);
    private static Stage window;
    private static Scene menu, jeu,liste;

    @Override
    public void start(Stage stage) throws Exception {
        window = stage;
        menu = new Scene(FXMLLoader.load(getClass().getResource("/Menu.fxml")));
        window.setScene(menu);
        window.setTitle("Flappy bird");
        window.show();

    }

    public static Scene getListe() {
        return liste;
    }
    public static Stage getWindow() {
        return window;
    }
    public static Scene getMenu() {
        return menu;
    }
    public static Scene getJeu() {
        return jeu;
    }


    public static void setJeu(Scene j) {
        jeu = j;
    }
    public static Manager getLeManager() {
        return leManager;
    }


    @Override
    public void stop() throws Exception {
        if (leManager.getPartie() != null) {
            leManager.stopBoucle();
            super.stop();
        }
    }
}
