package boucle;


public class BoucleurJeu extends Boucleur {

    /**
     * Méthode qui permet de former une boucle toute les 30 ms dans un thread
     */
    @Override
    public void run() {
        while (actif) {
            beep();
            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                actif = false;
            }
        }
    }
}
