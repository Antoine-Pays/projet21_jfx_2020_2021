package boucle;

import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe est un point d'extension pour tous les types de boucleur
 */
public abstract class Boucleur implements Runnable, Observable {

    private List<InvalidationListener> lesObservateurs = new ArrayList<>();

    protected boolean actif = false;

    /**
     * Récupère si la boucle est active ou pas
     * @return retourne un booleen si la boucle est active ou pas
     */
    public boolean isActif() {
        return actif;
    }

    /**
     * Permets de changer la valeur du booleen qui permet de savoir si la boucle est active ou pas
     * @param actif valeur booleen que souhaite appliquer
     */
    public void setActif(boolean actif) {
        this.actif = actif;
    }

    /**
     * Ajoute un listener à écouter
     * @param invalidationListener Listener à ajouter
     */
    @Override
    public void addListener(InvalidationListener invalidationListener) {
        lesObservateurs.add(invalidationListener);
    }

    /**
     * Supprime un listener à écouter
     * @param invalidationListener Listener à supprimer
     */
    @Override
    public void removeListener(InvalidationListener invalidationListener) {
        lesObservateurs.remove(invalidationListener);
    }

    /**
     * Sert à mettre à jour les actions effectuer dans l'invalidation listener
     */
    protected void beep(){
        lesObservateurs.forEach(o -> Platform.runLater(() -> o.invalidated(this)));
    }
}
