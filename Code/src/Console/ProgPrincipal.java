package Console;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import modele.Utilisateur;


import java.util.Comparator;


public class ProgPrincipal {

    public static void main(String [] args) throws Exception {

        creerUtilisateurs();
    }


    public static void creerUtilisateurs() {
        Utilisateur kami = new Utilisateur("kam", "kam", "/img/birdy.gif");
        Utilisateur anto = new Utilisateur("antoine", "anto", "/img/bird.png");
        ObservableList<Utilisateur> mesJoueurs = FXCollections.observableArrayList();
        kami.setRecord(2);
        anto.setRecord(10);
        mesJoueurs.add(kami);
        mesJoueurs.add(anto);

        mesJoueurs = mesJoueurs.sorted(Comparator.comparingInt(Utilisateur::getRecord).reversed());

        System.out.println(mesJoueurs);

    }
}


